/* Linux kernel thread x86 target support.

   Copyright (C) 2011-2016 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */


#include "defs.h"
#include "gdbcore.h"
#include "regcache.h"
#include "inferior.h"
#include "i386-tdep.h"
#include "amd64-tdep.h"


#include "linux-kthread.h"
#include "x86-linux-kthread.h"

/* Support for Linux kernel threads */

static struct inactive_task_frame {

        unsigned long r15;
        unsigned long r14;
        unsigned long r13;
        unsigned long r12;

        unsigned long bx;

        /*
         * These two fields must be together.  They form a stack frame header,
         * needed by get_frame_pointer().
         */
        unsigned long bp;
        unsigned long ret_addr;

        unsigned long ax;

        unsigned long cx;
        unsigned long dx;
        unsigned long si;
        unsigned long di;

        unsigned long r8;
        unsigned long r9;
        unsigned long r10;
        unsigned long r11;

        unsigned long ip;

        unsigned long rflags;

        unsigned long cs;
        unsigned long ss;
}
sleeping_task_frame;

static struct cpu_thread_save {
        unsigned long ax;
        unsigned long bx;
        unsigned long cx;
        unsigned long dx;
        unsigned long si;
        unsigned long di;
        unsigned long bp;
        unsigned long sp;

        unsigned long r8;
        unsigned long r9;
        unsigned long r10;
        unsigned long r11;
        unsigned long r12;
        unsigned long r13;
        unsigned long r14;
        unsigned long r15;

        unsigned long ip;

        unsigned long rflags;

        unsigned long cs;
        unsigned long ss;
        unsigned long ds;
        unsigned long es;

        unsigned long fsbase;

        unsigned long gsbase;

}
cpu_thread_save;

/* This function gets the register values that the schedule() routine
 * has stored away on the stack to be able to restart a sleeping task.
 *
 **/

static void
x86_linuxkthread_fetch_registers(struct regcache * regcache,
        int regnum, CORE_ADDR task_struct) {
        struct gdbarch * gdbarch = get_regcache_arch(regcache);
        enum bfd_endian byte_order = gdbarch_byte_order(gdbarch);

        CORE_ADDR sp = 0;

        int i;

        CORE_ADDR thread_info_addr;

        DECLARE_FIELD(thread_struct, sp);
        DECLARE_FIELD(thread_struct, es);
        DECLARE_FIELD(thread_struct, ds);
        DECLARE_FIELD(thread_struct, fsbase);
        DECLARE_FIELD(thread_struct, gsbase);

        DECLARE_FIELD(thread_struct, ax);
        DECLARE_FIELD(thread_struct, cx);
        DECLARE_FIELD(thread_struct, dx);
        DECLARE_FIELD(thread_struct, si);
        DECLARE_FIELD(thread_struct, di);
        DECLARE_FIELD(thread_struct, r8);
        DECLARE_FIELD(thread_struct, r9);
        DECLARE_FIELD(thread_struct, r10);
        DECLARE_FIELD(thread_struct, r11);
        DECLARE_FIELD(thread_struct, cs);
        DECLARE_FIELD(thread_struct, ss);
        DECLARE_FIELD(thread_struct, rflags);
        DECLARE_FIELD(thread_struct, ip);

        DECLARE_FIELD(task_struct, thread);

        gdb_byte cpu_thread_buf[F_SIZE(task_struct, thread)];

        gdb_assert(regnum >= -1);

        /*get thread_info address */
        thread_info_addr = task_struct;

        /*get cpu_context as saved by scheduled */
        read_memory((CORE_ADDR) thread_info_addr + F_OFFSET(task_struct, thread),
                (gdb_byte * ) & cpu_thread_buf, F_SIZE(task_struct, thread));

        cpu_thread_save.sp = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, sp));

        read_memory((CORE_ADDR) cpu_thread_save.sp,
                (gdb_byte * ) & sleeping_task_frame, sizeof(struct inactive_task_frame));

        for (i = 0; i < gdbarch_num_regs(target_gdbarch()) - 8; i++)
                regcache_raw_supply(regcache, i, & cpu_thread_save.ax);

        cpu_thread_save.ip = sleeping_task_frame.ret_addr;
        cpu_thread_save.bp = sleeping_task_frame.bp;
        cpu_thread_save.bx = sleeping_task_frame.bx;
        cpu_thread_save.r12 = sleeping_task_frame.r12;
        cpu_thread_save.r13 = sleeping_task_frame.r13;
        cpu_thread_save.r14 = sleeping_task_frame.r14;
        cpu_thread_save.r15 = sleeping_task_frame.r15;

        cpu_thread_save.es = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, es));
        cpu_thread_save.ds = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, ds));
        cpu_thread_save.fsbase = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, fsbase));
        cpu_thread_save.fsbase = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, gsbase));

        cpu_thread_save.cs = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, cs));
        cpu_thread_save.ss = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, ss));
        cpu_thread_save.rflags = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, rflags));
        cpu_thread_save.cx = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, cx));
        cpu_thread_save.dx = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, dx));
        cpu_thread_save.si = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, si));
        cpu_thread_save.di = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, di));
        cpu_thread_save.r8 = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, r8));
        cpu_thread_save.r9 = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, r9));
        cpu_thread_save.r10 = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, r10));
        cpu_thread_save.r11 = * (unsigned long * )(cpu_thread_buf + F_OFFSET(thread_struct, r11));
        
        regcache_raw_supply(regcache, AMD64_RIP_REGNUM, & cpu_thread_save.ip);
        regcache_raw_supply(regcache, AMD64_RBP_REGNUM, & cpu_thread_save.bp);
        regcache_raw_supply(regcache, AMD64_RBX_REGNUM, & cpu_thread_save.bx);
        regcache_raw_supply(regcache, AMD64_R12_REGNUM, & cpu_thread_save.r12);
        regcache_raw_supply(regcache, AMD64_R13_REGNUM, & cpu_thread_save.r13);
        regcache_raw_supply(regcache, AMD64_R14_REGNUM, & cpu_thread_save.r14);
        regcache_raw_supply(regcache, AMD64_R15_REGNUM, & cpu_thread_save.r15);

        regcache_raw_supply(regcache, AMD64_EFLAGS_REGNUM, & cpu_thread_save.rflags);

        regcache_raw_supply(regcache, AMD64_RSP_REGNUM, & cpu_thread_save.sp);
        regcache_raw_supply(regcache, AMD64_CS_REGNUM, & cpu_thread_save.cs);
        regcache_raw_supply(regcache, AMD64_SS_REGNUM, & cpu_thread_save.ss);
        regcache_raw_supply(regcache, AMD64_ES_REGNUM, & cpu_thread_save.es);
        regcache_raw_supply(regcache, AMD64_DS_REGNUM, & cpu_thread_save.ds);
        regcache_raw_supply(regcache, AMD64_FS_REGNUM, & cpu_thread_save.fsbase);
        regcache_raw_supply(regcache, AMD64_GS_REGNUM, & cpu_thread_save.gsbase);

        regcache_raw_supply(regcache, AMD64_RBX_REGNUM, & cpu_thread_save.ax);
        regcache_raw_supply(regcache, AMD64_RCX_REGNUM, & cpu_thread_save.cx);
        regcache_raw_supply(regcache, AMD64_RDX_REGNUM, & cpu_thread_save.dx);
        regcache_raw_supply(regcache, AMD64_RSI_REGNUM, & cpu_thread_save.si);
        regcache_raw_supply(regcache, AMD64_RDI_REGNUM, & cpu_thread_save.di);
        regcache_raw_supply(regcache, AMD64_R11_REGNUM, & cpu_thread_save.r11);
        regcache_raw_supply(regcache, AMD64_R10_REGNUM, & cpu_thread_save.r10);
        regcache_raw_supply(regcache, AMD64_R9_REGNUM, & cpu_thread_save.r9);
        regcache_raw_supply(regcache, AMD64_R8_REGNUM, & cpu_thread_save.r8);

        for (i = 0; i < gdbarch_num_regs(target_gdbarch()); i++)
                if (REG_VALID != regcache_register_status(regcache, i))
                /* Mark other registers as unavailable.  */
                        regcache_invalidate(regcache, i);

}

static void
x86_linuxkthread_store_registers(const struct regcache * regcache,
        int regnum, CORE_ADDR addr) {
        struct gdbarch * gdbarch = get_regcache_arch(regcache);
        enum bfd_endian byte_order = gdbarch_byte_order(gdbarch);

        /* TODO */
        gdb_assert(regnum >= -1);
        gdb_assert(0);

}

/* get_unmapped_area() in linux/mm/mmap.c.  */
DECLARE_ADDR(get_unmapped_area);

#define DEFAULT_PAGE_OFFSET 0xffff880000000000

void x86_linuxkthread_get_page_offset(CORE_ADDR * page_offset) {
        const char * result = NULL;

        /* We can try executing a python command if it exists in the kernel
            source, and parsing the result.
            result = execute_command_to_string ("lx-pageoffset", 0); */

        /* Find CONFIG_PAGE_OFFSET macro definition at get_unmapped_area symbol
           in linux/mm/mmap.c.  */

        result = kthread_find_macro_at_symbol( & get_unmapped_area,
                "CONFIG_PAGE_OFFSET");
        if (result) { * page_offset = strtol(result, (char * * ) NULL, 16);
        } else {
                /* Kernel is compiled without macro info so make an educated guess.  */
                
                warning("Assuming PAGE_OFFSET is 0x%p. Disabling to_interrupt\n",
                        DEFAULT_PAGE_OFFSET);
                        
                /* PAGE_OFFSET can't be reliably determined so disable the target_ops
	 			  to_interrupt ability. This means target can onbly be halted via
	 			  a breakpoint set in the kernel, which will mean CPU is configured
	 			  for kernel memory view.  */
	 							
                lkthread_disable_to_interrupt = 1; * page_offset = DEFAULT_PAGE_OFFSET;
        }

        return;
}

static int x86_linuxkthread_is_kernel_address(const CORE_ADDR addr) {
        static CORE_ADDR linux_page_offset;

        if (!linux_page_offset)
                x86_linuxkthread_get_page_offset( & linux_page_offset);

        return (addr >= linux_page_offset) ? true : false;
}

/* The linux_kthread_arch_ops for most x86 targets.  */

static struct linux_kthread_arch_ops x86_linuxkthread_ops = {
        x86_linuxkthread_fetch_registers,
        x86_linuxkthread_store_registers,
        x86_linuxkthread_is_kernel_address,
};

/* Register x86_linuxkthread_ops in GDBARCH.  */

void
register_x86_linux_kthread_ops(struct gdbarch * gdbarch) {
        set_gdbarch_linux_kthread_ops(gdbarch, & x86_linuxkthread_ops);
}

